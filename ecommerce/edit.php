<!-- Jules barbolino-->
<?php 
	include('connection.php');

	if (isset($_POST['update'])) {
		$id = $_POST['id'];
		$fname = $_POST['fname'];
    $lname = $_POST['lname'];
		$contact = $_POST['contact'];
    $email = $_POST['email'];

		$result = mysqli_query($conn, "UPDATE user SET fname='$fname', lname='$lname', contact='$contact', email='$email' WHERE id=$id");

			header("Location: index.php");
	}
 ?>
 <?php 

 	$id = $_GET['id'];
 	$result = mysqli_query($conn, "SELECT * FROM user WHERE id=$id");

 	while ($res = mysqli_fetch_array($result)) {
 		$fname = $res['fname'];
    $lname = $res['lname'];
 		$contact = $res['contact'];
    $email = $res['email'];
 	}
  ?>
  <!DOCTYPE html>
  <html>
  <head>
  	<title>Edit Data</title>
  </head>
  <body>
  	<a href="index.php">Home</a>
  	<br/><br/>
  	<div>
  		<form method="post" action="edit.php">
  			<table border="0">
  				<tr>
  					<td>First name:</td>
  					<td><input type="text" name="fname" pattern="[A-Z][a-z]*" required value="<?php echo $fname;?>"></td>
  				</tr>
          <tr>
            <td>Last name:</td>
            <td><input type="text" name="lname" pattern="[A-Z][a-z]*" required value="<?php echo $lname;?>"></td>
          </tr>
  				<tr>
  					<td>Contact no:</td>
  					<td><input type="text" name="contact" pattern="[0-9]{11}" required value="<?php echo $contact;?>"></td>
  				</tr>
          <tr>
            <td>Email</td>
            <td><input type="email" name="email" required value="<?php echo $email;?>"></td>
          </tr>
  				<tr>
  					<td><input type="hidden" name="id" value="<?php echo $_GET['id'];?>"></td>
  					<td><input type="submit" name="update" value="Update"></td>
  				</tr>
  			</table>
  		</form>
  	</div>
  </body>
  </html>