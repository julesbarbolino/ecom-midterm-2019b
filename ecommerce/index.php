<!-- Jules barbolino-->
<?php 
	include('connection.php');
	$result = mysqli_query($conn, "SELECT * FROM user ORDER BY id DESC");
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Simple CRUD Program</title>
</head>
<body>
	<select>
		<option value="">Default</option>
	  	<option name="asc" value="1">Ascending</option>
	  	<option name="desc" value="1">Descending</option>
	</select>
	<dir class="" style="float: right; padding-right: 20%;">
		<input type="button" name="" value="+ ADD" onclick="location.href='add.html';">
	</dir>
	<br/><br/>
	<table width='80%' border=0>
		<tr bgcolor="#CCCCCC">
			<td>Name</td>
			<td> </td>
			<td>Contact No.</td>
			<td>Email</td>
			<td></td>
		</tr>
	<?php 
		while ($res = mysqli_fetch_array($result)) {
			echo "<tr>";
			echo "<td>".$res['lname']."</td>";
			echo "<td>".$res['fname']."</td>";
			echo "<td>".$res['contact']."</td>";
			echo "<td>".$res['email']."</td>";
			echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a> | <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";
		}
	 ?>
	</table>
</body>
</html>